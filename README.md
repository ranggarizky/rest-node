sebelum akses api harap registrasi user terlebih dahulu
new user
- POST api/users [username,password]

get user
- GET api/users

Akses api ----------------------------------------------------------------------------

get all contact
- GET api/contact

get contact
- GET api/contact/{id}

new contact
- POST api/contact [name,email,phone,address,title,company]

update contact
- PUT api/contact/{id}

delete contact
- DELETE api/contact/{id}

search contact
- GET api/contact/search/{searchby}/{value}
search by = [name,email,phone,address,title,company]

